package freund.test.grid;

import demo.grid.MyServerOperations;
import demo.grid.MyServerPOA;
import demo.grid.MyServerPackage.MyException;

import java.math.BigDecimal;

public class GridImpl extends MyServerPOA {

    private final MyServerOperations myServerOperations;

    public GridImpl(MyServerOperations myServerOperations) {
        this.myServerOperations = myServerOperations;
    }

    @Override
    public short height() {
        return myServerOperations.height();
    }

    @Override
    public short width() {
        return myServerOperations.width();
    }

    @Override
    public void set(short n, short m, BigDecimal value) {
        myServerOperations.set(n, m, value);
    }

    @Override
    public BigDecimal get(short n, short m) {
        return myServerOperations.get(n, m);
    }

    @Override
    public short opWithException() throws MyException {
        return myServerOperations.opWithException();
    }
}
