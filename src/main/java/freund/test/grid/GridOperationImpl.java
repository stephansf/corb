package freund.test.grid;

import demo.grid.MyServerOperations;
import demo.grid.MyServerPackage.MyException;


import java.math.BigDecimal;

public class GridOperationImpl implements MyServerOperations {
    private short height = 31;
    private short widht = 14;
    private BigDecimal grid[][] = new BigDecimal[height][widht];

    public GridOperationImpl() {
        for (int h = 0; h < height; h++)
            for (int w = 0; w < widht; w++)
                grid[h][w] = new BigDecimal("0.21");
    }

    @Override
    public short height() {
        return height;
    }

    @Override
    public short width() {
        return widht;
    }

    @Override
    public void set(short n, short m, BigDecimal value) {
        if (n <= height && m <= widht)
            grid[n][m] = value;
    }

    @Override
    public BigDecimal get(short n, short m) {
        if (n <= height && m <= widht)
            return grid[n][m];
        return new BigDecimal("0.01");
    }

    @Override
    public short opWithException() throws MyException {
        throw new MyException("Este é um teste");
    }
}
