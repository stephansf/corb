package freund.test.grid;

import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class TieServer {


    public static void main(String[] args) throws Exception {
        ORB orb = ORB.init(args, null);
        Object rootPOA = orb.resolve_initial_references("RootPOA");
        POA poa = POAHelper.narrow(rootPOA);
        poa.the_POAManager().activate();
        GridImpl gridImpl = new GridImpl(new GridOperationImpl());
        Object corbaObject = poa.servant_to_reference(gridImpl);
        PrintWriter ps = new PrintWriter(new FileOutputStream(new File(args[0])));
        ps.println(orb.object_to_string(corbaObject));
        ps.close();
        if(args.length == 2) {
            File killFile = new File(args[1]);
            while (!killFile.exists()) {
                Thread.sleep(1000);
            }
            orb.shutdown(true);
        } else {
            orb.run();
        }
    }
}