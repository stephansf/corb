package freund.test.books.server;

import book.CollectionPOA;
import book.FuzzyBoolean;
import org.omg.PortableServer.POA;

/**
 * In the context of the Portable Object Adapter (POA), the implementation class is known as a servant class. A servant class provides the code that implements CORBA objects.
 * At the root of the hierarchy is org.omg.PortableServer.Servant, which is the base of all Java servant classes.
 * <p>
 * The CollectionImpl servant class inherits from BookRepository.CollectionPOA, which indicates that the servant implements the BookRepository::Collection IDL interface. The BookRepository.CollectionPOA class is an abstract class, declared in the skeleton code, and its methods must be overridden to implement each of the IDL attributes and operations.
 */
public class CollectionImpl extends CollectionPOA {

    private int numberOfBooks;
    private String nameOfCollection;
    private POA poa;

    public CollectionImpl(POA poa) {
        this.poa = poa;
        this.numberOfBooks = 1000;
        this.nameOfCollection = "Freund Collection =)";
    }

    @Override
    public int numberOfBooks() {
        return numberOfBooks;
    }

    @Override
    public String nameOfCollection() {
        return nameOfCollection;
    }

    @Override
    public void nameOfCollection(String nameOfCollection) {
        this.nameOfCollection = nameOfCollection;
    }

    @Override
    public FuzzyBoolean isInCollection(int book_id) {
        return FuzzyBoolean.UNKNOWN;
    }

    public POA getPoa() {
        return poa;
    }
}
