package freund.test.books.client;

import book.Collection;
import book.CollectionHelper;
import freund.test.books.Constants;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * The client connects to the Book Repository server and remotely invokes a couple of attributes and an operation to retrieve information about a Collection CORBA object.
 * 1.
 * Initialize the ORB— The first thing the client must do is create an ORB object, global_orb, calling CORBA::ORB_init() in C++ and org.omg.CORBA.ORB.init() in Java. The array of command-line arguments, argv in C++ and args in Java, is passed to the initialization function, giving the user an opportunity to pass parameters to the ORB. In Java, the ORB options must be explicitly removed, using remove_ORB_args(), before processing the rest of the command-line arguments. See the section “ORB Initialization,” later in this chapter.
 *
 * 2.
 * Obtain an object reference— To locate a remote CORBA object, the client has to obtain an object reference. The object reference contains complete location details for the object. For example, on a TCP/IP network the object reference contains the server host and IP port.
 *
 * In this example, the client reads the object reference, which is in stringified form, from a well-known file. A more sophisticated way of passing the object reference from server to client is to use the CORBA Naming Service, which is demonstrated in Example 5.
 *
 * The read_reference() function reads the stringified object reference from a file and converts the string to an object reference using string_to_object(). The return type of string_to_object() is CORBA::Object in C++ and org.omg.CORBA.Object in Java, which is the base class for all object reference types.
 *
 * The object reference returned by read_reference() is cast to the correct type using Collection::_narrow() in C++ and CollectionHelper.narrow() in Java. The narrow function makes a down cast, similar to dynamic_cast<Collection> in C++ and (Collection) in Java. Narrowing does more than that, however—it also checks that the down cast is legal with respect to the IDL inheritance hierarchy.
 *
 * In C++, a failed narrow returns a nil object reference that must be checked for using the CORBA::is_nil() function. In Java, a failed narrow raises the CORBA::BAD_PARAM system exception.
 *
 * 3.
 * Make remote invocations— Use the remote object reference, collectionV in C++ and theCollection in Java, to begin making remote invocations. You can invoke any of the operations or attributes declared in the Collection IDL interface.
 *
 * In C++, the Collection object reference is declared as a smart pointer type, BookRepository::Collection_var. The purpose of the _var types is to help you avoid memory leaks: The destructor of the _var type deletes the memory it is pointing at. The Collection_var type is designed to mimic the syntax of the Collection* pointer type—for example, the members of collectionV can be accessed using the -> member access operator.
 *
 * 4.
 * Shut down the ORB— The ORB must shut down correctly so that it can close connections in an orderly manner and release any other resources it is using. Two calls, CORBA::ORB::shutdown() and CORBA::ORB::destroy(),complete the shutdown.
 */
public class BookCollectionClient {
    private static ORB GLOBAL_ORB = null;

    public static void main(String[] args) {
        try {
            GLOBAL_ORB = ORB.init(args, null);
            //TODO tem que remover os parametros do corba em JAVA
            String appArgs[] = args;
            Collection bookCollection = getCollectionFromOrb();
            readParams(bookCollection);
            setNewParamsAndRead(bookCollection);
        } catch (Exception e) {
            System.out.println("Unexpected CORBA exception");
            e.printStackTrace();
        } finally {
            GLOBAL_ORB.shutdown(true);
            GLOBAL_ORB.destroy();
        }

    }

    /**
     * The object reference returned by readReference() is cast to the correct type using Collection::_narrow() in C++ and CollectionHelper.narrow() in Java. The narrow function makes a down cast, similar to dynamic_cast<Collection> in C++ and (Collection) in Java. Narrowing does more than that, however—it also checks that the down cast is legal with respect to the IDL inheritance hierarchy
     */
    private static Collection getCollectionFromOrb() {
        Object readReference = readReference(Constants.BOOK_COLLECTION_REF);
        Collection bookCollection = CollectionHelper.narrow(readReference);
        return bookCollection;
    }

    /**
     * Le a referencia de um arquivo
     *
     * @param file
     * @return Obtain an object reference— To locate a remote CORBA object, the client has to obtain an object reference. The object reference contains complete location details for the object. For example, on a TCP/IP network the object reference contains the server host and IP port.
     * <p>
     * In this example, the client reads the object reference, which is in stringified form, from a well-known file. A more sophisticated way of passing the object reference from server to client is to use the CORBA Naming Service, which is demonstrated in Example 5.
     * <p>
     * The read_reference() function reads the stringified object reference from a file and converts the string to an object reference using string_to_object(). The return type of string_to_object() is CORBA::Object in C++ and org.omg.CORBA.Object in Java, which is the base class for all object reference types.
     */
    private static Object readReference(String file) {
        System.out.println("Lendo stringfied Object do arquivo: " + file);
        try (FileReader fileReader = new FileReader(file);
             BufferedReader in = new BufferedReader(fileReader)) {
            String stringfiedObject = in.readLine();
            return GLOBAL_ORB.string_to_object(stringfiedObject);
        } catch (IOException e) {
            System.out.println("Nao foi possivel ler o arquivo");
            e.printStackTrace();
            return null;
        }
    }

    private static void readParams(Collection bookCollection) {
        System.out.println("Details of book collection:");
        String nameOfCollection = bookCollection.nameOfCollection();
        System.out.println("Name of collection: " + nameOfCollection);
        int numberOfBooks = bookCollection.numberOfBooks();
        System.out.println("Number of books: " + numberOfBooks);
    }

    private static void setNewParamsAndRead(Collection bookCollection) {
        System.out.println("Changing Name Of Book Collection");
        bookCollection.nameOfCollection("Brand New Collection");
        System.out.println("Checking Name of Collection");
        String collectionName = bookCollection.nameOfCollection();
        System.out.println("New Collection name: " + collectionName);
    }
}
